from django.db import models
from django.contrib import auth
from django.core.files.base import ContentFile
import random
import string
from django.core.mail import send_mail

def generate_key(key_len):
    pool = string.ascii_lowercase+string.digits
    return ''.join(random.SystemRandom().choice(pool)
                   for _ in xrange(key_len))

class UserManager(auth.models.UserManager):
    key_len = 16
    
    def send_confirmation_email(self, user):
        subject = 'Textbin Confirmation Link'
        link = ('<a href="http://localhost:8000/notes/do_confirm/'+
                user.confirmation_key+`user.id`+'"> %s </a>')
        body = ('<html><body>Please follow '+link%('this link')
                +'to confirm your account</body></html>');
        send_mail(subject, None,
                  'noreply@texbin.com', [user.email],
                  html_message=body, fail_silently=False)

    def create_user(self, *args, **kwargs):
        key = generate_key(self.key_len)
        user = super(UserManager, self).create_user(*args, **kwargs)
        user.confirmation_key = key
        user.is_active=False
        user.save()
        self.send_confirmation_email(user)
        return user

    
    def confirm_user(self, confirmation_string):
        confirmation_key = confirmation_string[:self.key_len]
        user_id = confirmation_string[self.key_len:]
        user = self.get(id=int(user_id))
        if user.confirmation_key==confirmation_key:
            user.is_active = True
            user.save()
            return True
        return False
        


class User(auth.models.User):
    confirmation_key = models.CharField(max_length=20)
    objects=UserManager()
        
class Folder(models.Model):
    owner = models.ForeignKey(auth.models.User)
    name = models.CharField(max_length=200)
    
    def __unicode__(self):
        return self.name

    
class NoteManager(models.Manager):
    def create_note(self, content, *args, **kwargs):
        note = self.create(*args, **kwargs)
        note.text_file.save(name=note.name,
                            content=ContentFile(content))
        return note

    
class Note(models.Model):
    owner = models.ForeignKey(auth.models.User)
    name = models.CharField(max_length=200)
    folder = models.ForeignKey(Folder)
    text_file = models.FileField()
    is_public = models.BooleanField(default=False)

    objects = NoteManager()
    
    def __unicode__(self):
        return self.text_file.name
