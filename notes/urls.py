from django.conf.urls import patterns, url
from notes import views

urlpatterns = patterns('',
    url(r'^home/$', views.home, name='home'),
    url(r'^signup/$', views.signup, name='signup'),
    url(r'^do_signup/$', views.do_signup, name='do_signup'),
    url(r'^login/$', views.login, name='login'),
#    url(r'^do_login/$', views.do_login, name='do_login'),
    url(r'^add_note/$', views.add_note, name='add_note'),
    url(r'^do_add_note/$', views.do_add_note, name='do_add_note'),
    url(r'^add_folder/$', views.add_folder, name='add_folder'),
    url(r'^do_add_folder/$', views.do_add_folder, name='do_add_folder'),
    url(r'^folders/$', views.folders_index, name='folders_index'),
    url(r'^folders/([0-9]+)/$', views.folders, name='folders'),
    url(r'^notes/$', views.notes_index, name='notes_index'),
    url(r'^notes/([0-9]+)/$', views.notes, name='notes'),
    url(r'^logout/$', views.do_logout, name='do_logout'),
    url(r'^move_note/$', views.move_note, name='move_note'),
    url(r'^do_move_note/$', views.do_move_note, name='do_move_note'),
    url(r'^do_confirm/([0-9a-z]+)$', views.do_confirm, name='do_confirm'),
)
