from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.contrib import auth
from .models import User, Folder, Note
from django.core.mail import send_mail
from django.core.files import File
from .forms import LoginForm

@login_required(redirect_field_name=None)
def home(request):
    return HttpResponse('placeholder')


def signup(request, display_error=0):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('notes:home'))
        
    error = 'There was an error in your form' if display_error \
      else None
    return render(request, 'notes/signup.html',
                  {'error_message':error})


def do_signup(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('notes:home'))

    new_username = request.POST['username']
    new_password = request.POST['password']
    new_email_addr = request.POST['email']

    new_user = User.objects.create_user(username=new_username,
                    email=new_email_addr)
    new_user.set_password(new_password)
    new_user.save()
    print new_user.email

    return HttpResponseRedirect(reverse('notes:login'))


def login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('notes:home'))

    if request.method=='POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username=form.cleaned_data['username']
            password=form.cleaned_data['password']
            user=auth.authenticate(username=username,
                                   password=password)
            if user is None:
                return HttpResponse('Wrong username or password')
            if not user.is_active:
                return HttpResponse('Confirm your account first')
            auth.login(request, user)
            return HttpResponseRedirect(reverse('notes:home'))
        else:
            return render(request, 'notes/login.html',
                          {'form':form})
    else:
        form = LoginForm()
        return render(request, 'notes/login.html',
                      {'form':form})


@login_required(redirect_field_name=None)
def do_logout(request):
    auth.logout(request)
    return HttpResponseRedirect(reverse('notes:login'))

# def do_login(request):
#     if request.user.is_authenticated():
#         return HttpResponseRedirect(reverse('notes:home'))

#     user = auth.authenticate(username=request.POST['username'],
#                              password=request.POST['password'])
#     if user is None:
#         return HttpResponse(`request.POST['username']`+ ' '+
#                             `request.POST['password']`)
    
#     if user.is_active:
#         auth.login(request, user)
#         return HttpResponseRedirect(reverse('notes:home'))
#     else:
#         return HttpResponse('Confirm your account first')


@login_required(redirect_field_name=None)
def add_folder(request):
    return render(request, 'notes/add_folder.html')


@login_required(redirect_field_name=None)
def do_add_folder(request):
    new_folder = Folder(name=request.POST['folder_name'],
                        owner=request.user)
    new_folder.save()
    return HttpResponseRedirect(reverse('notes:home'))


@login_required(redirect_field_name=None)
def add_note(request):
    return render(request, 'notes/add_note.html')


@login_required(redirect_field_name=None)
def do_add_note(request):
    dest_folder = get_object_or_404(Folder,
                               name=request.POST['folder_name'])
    public = request.POST.has_key('public')
    new_note=Note.objects.create_note(name=request.POST['note_name'],
                                 owner=request.user,
                                 folder=dest_folder,
                                 is_public=public,
                                 content=request.POST['note_content'])
    
    return HttpResponseRedirect(reverse('notes:home'))


@login_required(redirect_field_name=None)
def folders_index(request):
    return render(request, 'notes/folders_index.html',
                  {'folders':request.user.folder_set.all()})


@login_required(redirect_field_name=None)
def folders(request, folder_id):
    folder = get_object_or_404(Folder, id=folder_id)
    if folder.owner.id != request.user.id:
        return HttpResponseForbidden('403 Forbidden')()
    return render(request, 'notes/folders.html',
                  {'name':folder.name,
                   'notes':folder.note_set.all()})


@login_required(redirect_field_name=None)
def notes_index(request):
    return render(request, 'notes/notes_index.html',
                  {'notes':request.user.note_set.all()})


def notes(request, note_id):
    note = get_object_or_404(Note, id=note_id)
    if not note.is_public and note.owner.id != request.user.id:
        return HttpResponseForbidden('403 Forbidden')
    content = ''.join(note.text_file.readlines())
    return render(request, 'notes/notes.html',
                  {'name':note.name,
                   'content':content})
    
@login_required(redirect_field_name=None)
def home(request):
    return HttpResponse('placeholder')

@login_required(redirect_field_name=None)
def move_note(request):
    return render(request, 'notes/move_note.html')

@login_required(redirect_field_name=None)
def do_move_note(request):
    note = get_object_or_404(Note, id=int(request.POST['note_id']))
    folder = get_object_or_404(Folder,
                               id=int(request.POST['dest_id']))

    if (note.owner.id != request.user.id or
        folder.owner.id != note.owner.id):
        return HttpResponseForbidden('403 Forbidden')

    note.folder = folder
    note.save()

    return HttpResponseRedirect(reverse('notes:home'))

def do_confirm(request, confirmation_string):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('notes:home'))

    User.objects.confirm_user(confirmation_string)

    return HttpResponseRedirect(reverse('notes:login'))
