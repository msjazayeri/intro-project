from django import forms
from django.core import validators

class LoginForm(forms.Form):
    username=forms.CharField(label='Userame',
                             validators=[validators.validate_slug],
                             max_length=200)
    password=forms.CharField(label='Password',
                             validators=[validators.validate_slug],
                             max_length=200,
                             widget=forms.PasswordInput)
